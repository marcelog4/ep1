#ifndef CANOA_HPP
#define CANOA_HPP

#include "barco.hpp"

using namespace std;

class Canoa : public Barco{
        private:
                int hate_mode;
        public:
                Canoa();
                Canoa(int x, int y, int valor_mapa, char orienta, int hate_mode, int comprimento);
                ~Canoa();

                int get_hate_mode();
                void set_hate_mode(int hate_mode);
};

#endif