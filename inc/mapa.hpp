#ifndef MAPA_HPP
#define MAPA_HPP
#include <vector>
#include "barco.hpp"
#include "submarino.hpp"
#include "porta_aviao.hpp"
#include "canoa.hpp"


using namespace std;

class Mapa
{
  private:
    string linha;
    unsigned int conta1, conta2, conta3, conta4, conta5;
    string url;
    int letra = 65;

  public:
    Mapa();
    ~Mapa();

    vector<Barco *> barcos;
    vector<Submarino *> submarinos;
    vector<Porta_aviao *> porta_avioes;
    vector<Canoa *> canoas;

    string barco;
    string orienta;
    string pos_x;
    string pos_y;

    char mapa_visivel[13][13];
    int mapa_oculto[13][13];
    
    int conta_mapa();
    void ler_mapa(int jogador_n);
    void posiciona_barcos(vector<Barco *> barcos);

    void cria_mapa();
    void cria_mapa_oculto();

    bool colisao(int x,int y);
    int verifica_vector(int x, int y,int t);
    bool numero_aleatorio(int aux);
    bool verifica_vitoria();

    void imprime_mapa_visivel();
    void imprime_mapa_oculto();
};

#endif