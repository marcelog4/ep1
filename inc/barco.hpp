#ifndef BARCO_HPP
#define BARCO_HPP

#include <string>

using namespace std;

class Barco{
        private:
                int comprimento;
                int local_x;
                int local_y;
                int valor_mapa;
                char orienta;
        public:
                Barco();
                ~Barco();

                int get_comprimento();
                void set_comprimento(int comprimento);

                int get_valor_mapa();
                void set_valor_mapa(int valor_mapa);

                int get_local_x();
                void set_local_x(int local_x);

                int get_local_y();
                void set_local_y(int local_y);

                char get_orienta();
                void set_orienta(char orienta);

                void imprime_dados();

};

#endif
