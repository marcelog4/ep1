#ifndef PORTA_AVIAO_HPP
#define PORTA_AVIAO_HPP

#include "barco.hpp"

using namespace std;

class Porta_aviao : public Barco{
        private:
                int hit_chance;

        public:
                Porta_aviao();
                Porta_aviao(int x, int y, int valor_mapa, char orienta, int hit_chance , int comprimento);
                ~Porta_aviao();

                int get_hit_chance();
                void set_hit_chance(int hit_chance);

};

#endif

