#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "barco.hpp"

using namespace std;

class Submarino : public Barco{
        private:
                int escudo;

        public:
                Submarino();
                Submarino(int x, int y, int valor_mapa, char orienta, int escudo, int comprimento);
                ~Submarino();

                int get_escudo();
                void set_escudo(int escudo);
};

#endif
