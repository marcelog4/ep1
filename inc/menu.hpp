#ifndef MENU_HPP
#define MENU_HPP

class Menu{
    private:
        int opcao;
        int dificuldade;

    public:
        Menu();
        ~Menu();

        int get_opcao();
        void set_opcao(int opcao);
        int get_dificuldade();
        void set_dificuldade(int dificuldade);

        void escolha();
        void opcao_usuario();
        void imprime_menu();
        void imprime_dados();
        void escolhe_inimigo();
        void escolhe_dificuldade();

};

#endif