#ifndef COMPUTADOR_HPP
#define COMPUTADOR_HPP

#include "jogador.hpp"

using namespace std;

class Computador : public Jogador
{
      private:
        int dificuldade;

      public:
        Computador();
        ~Computador();

        int i, ii;

        int matriz_oculta_aux[13][13];
        char matriz_visivel_aux[13][13];

        int get_dificuldade();
        void set_dificuldade(int dificuldade);

        int facil();
        int medio();
        int dificil();
        int expert();

        void input_jogador();
        void copia_matriz();
        void inicia_jogador(int dificuldade);
};

#endif