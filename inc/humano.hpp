#ifndef HUMANO_HPP
#define HUMANO_HPP

#include "jogador.hpp"

using namespace std;

class Humano : public Jogador{
        public:
                Humano();
                ~Humano();

                int configura_input(char aux);
                void input_jogador();
                void inicia_jogador();
                
};

#endif