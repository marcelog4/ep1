#ifndef JOGADOR_HPP
#define JOGADOR_HPP
#include "mapa.hpp"

class Jogador{
    private:
        int jogador_n;
        int tecla_x;
        int tecla_y;

    public:
        Jogador();
        ~Jogador();

        int x,y;
        Mapa mapa;

        int get_jogador_n();
        void set_jogador_n(int jogador_n);
        int get_tecla_x();
        void set_tecla_x(int tecla_x);
        int get_tecla_y();
        void set_tecla_y(int tecla_y);

        virtual int configura_input(char aux);
        virtual void inicia_jogador();
        virtual void input_jogador();
        bool turno();
        void ganhou();
};

#endif