#include <iostream>
#include <fstream>
#include "mapa.hpp"

Mapa::Mapa()
{
        //cout << "classe mapa construida" << endl;
}

Mapa::~Mapa()
{
        //cout << "classe mapa destruida" << endl;
}

void Mapa::cria_mapa()
{
        for (conta1 = 0; conta1 < 13; conta1++)
        {
                for (conta2 = 0; conta2 < 13; conta2++)
                {
                        mapa_visivel[conta1][conta2] = '~';
                }
        }
}

void Mapa::cria_mapa_oculto()
{
        for (conta1 = 0; conta1 < 13; conta1++)
        {
                for (conta2 = 0; conta2 < 13; conta2++)
                {
                        mapa_oculto[conta1][conta2] = 0;
                }
        }
}

int Mapa::conta_mapa()
{
        ifstream arqe;
        url = "doc/map_1.txt";
        arqe.open(url);
        unsigned int ii = 0;

        while (getline(arqe, linha))
        {
                if (linha[0] != '#' && linha != "")
                {
                        ii++;
                }
        }
        arqe.close();
        return ii;
}

void Mapa::ler_mapa(int jogador)
{
        ifstream arqe;
        url = "doc/map_1.txt"; //direciona a pasta do arquivo
        cria_mapa_oculto();
        cria_mapa();
        unsigned int ii, iii, nline = 0,n,x,y;

        arqe.open(url);

        if (jogador == 1)
        {
                iii = (conta_mapa() / 2);
                ii = 0;
        }
        if (jogador == 2)
        {
                iii = conta_mapa();
                ii = (conta_mapa() / 2);
        }

        while (getline(arqe, linha))
        {
                if (linha[0] != '#' && linha != "")
                { //faz com que sejam apenas lido as linhas necessarias no arquivo
                        if (nline >= ii && nline < iii)
                        {
                                pos_x[1] = '\0';
                                pos_y[1] = '\0'; //faz com que a string tenha 1 valor , para nao ocorrer sobrescrita
                                for (n = 0, conta1 = 0, conta2 = 0, conta3 = 0, conta4 = 0, conta5 = 0; conta1 < linha.size(); conta1++)
                                {
                                        if (linha[conta1] == ' ')
                                                n++;

                                        if (n == 0 && linha[conta1] != ' ')
                                        {
                                                pos_y[conta5] = linha[conta1];
                                                y = stoi(pos_y); //transforma a string pos_y em um int y
                                                conta5++;
                                        }

                                        if (n == 1 && linha[conta1] != ' ')
                                        {
                                                pos_x[conta2] = linha[conta1];
                                                x = stoi(pos_x); //transforma a string pos_x em um int x
                                                conta2++;
                                        }

                                        if (n == 2 && linha[conta1] != ' ')
                                        {
                                                barco[conta3] = linha[conta1];
                                                conta3++;
                                        }
                                        if (n == 3 && linha[conta1] != ' ')
                                        {
                                                orienta[conta4] = linha[conta1];
                                                if (barco[0] == 's' && conta4 == 0)
                                                {
                                                        Submarino *submarino = new Submarino(x, y, 2, orienta[0], 1, 2);
                                                        barcos.push_back(submarino);
                                                        submarinos.push_back(submarino);
                                                }
                                                if (barco[0] == 'c' && conta4 == 0)
                                                {
                                                        Canoa *canoa = new Canoa(x, y, 1, orienta[0], 40, 1);
                                                        barcos.push_back(canoa);
                                                        canoas.push_back(canoa);
                                                }
                                                if (barco[0] == 'p' && conta4 == 0)
                                                {
                                                        Porta_aviao *porta_aviao = new Porta_aviao(x, y, 3, orienta[0], 50, 4);
                                                        barcos.push_back(porta_aviao);
                                                        porta_avioes.push_back(porta_aviao);
                                                }
                                                conta4++;
                                        }
                                }
                                //cout << y << " " << x << " " << nline << endl; //cout de teste
                        }
                        nline++;
                }
        }
        arqe.close();
        posiciona_barcos(barcos);
}

void Mapa::posiciona_barcos(vector<Barco *> barcos)
{
        int ii;
        unsigned int i;
        for (i = 0; i < barcos.size(); i++)
        {
                switch (barcos[i]->get_orienta())
                {
                case 'e':
                        for (ii = 0; ii < barcos[i]->get_comprimento(); ii++)
                        {
                                mapa_oculto[barcos[i]->get_local_y()][barcos[i]->get_local_x() - ii] = barcos[i]->get_valor_mapa();
                                if (barcos[i]->get_comprimento() == 2 && ii!=0) //posiciona os barcos que tem orientacao
                                {
                                        Submarino *submarino = new Submarino(barcos[i]->get_local_x() - ii, barcos[i]->get_local_y(), 2, barcos[i]->get_orienta(), 1, 2);
                                        submarinos.push_back(submarino);
                                }
                                else if (barcos[i]->get_comprimento() == 4 && ii!=0)
                                {
                                        Porta_aviao *porta_aviao = new Porta_aviao(barcos[i]->get_local_x() - ii, barcos[i]->get_local_y(), 3, barcos[i]->get_orienta(), 50, 4);
                                        porta_avioes.push_back(porta_aviao);
                                }
                        }
                        break;
                case 'd':
                        for (ii = 0; ii < barcos[i]->get_comprimento(); ii++)
                        {
                                mapa_oculto[barcos[i]->get_local_y()][barcos[i]->get_local_x() + ii] = barcos[i]->get_valor_mapa();
                                if (barcos[i]->get_comprimento() == 2 && ii!=0)
                                {
                                        Submarino *submarino = new Submarino(barcos[i]->get_local_x() + ii, barcos[i]->get_local_y(), 2, barcos[i]->get_orienta(), 1, 2);
                                        submarinos.push_back(submarino);
                                }
                                else if (barcos[i]->get_comprimento() == 4 && ii!=0)
                                {
                                        Porta_aviao *porta_aviao = new Porta_aviao(barcos[i]->get_local_x() + ii, barcos[i]->get_local_y(), 3, barcos[i]->get_orienta(), 50, 4);
                                        porta_avioes.push_back(porta_aviao);
                                }
                        }
                        break;
                case 'c':
                        for (ii = 0; ii < barcos[i]->get_comprimento(); ii++)
                        {
                                mapa_oculto[barcos[i]->get_local_y() - ii][barcos[i]->get_local_x()] = barcos[i]->get_valor_mapa();
                                if (barcos[i]->get_comprimento() == 2 && ii!=0)
                                {
                                        Submarino *submarino = new Submarino(barcos[i]->get_local_x(), barcos[i]->get_local_y() - ii, 2, barcos[i]->get_orienta(), 1, 2);
                                        submarinos.push_back(submarino);
                                }
                                else if (barcos[i]->get_comprimento() == 4 && ii!=0)
                                {
                                        Porta_aviao *porta_aviao = new Porta_aviao(barcos[i]->get_local_x(), barcos[i]->get_local_y() - ii, 3, barcos[i]->get_orienta(), 50, 4);
                                        porta_avioes.push_back(porta_aviao);
                                }
                        }
                        break;
                case 'b':
                        for (ii = 0; ii < barcos[i]->get_comprimento(); ii++)
                        {
                                mapa_oculto[barcos[i]->get_local_y() + ii][barcos[i]->get_local_x()] = barcos[i]->get_valor_mapa();
                                if (barcos[i]->get_comprimento() == 2 && ii!=0)
                                {
                                        Submarino *submarino = new Submarino(barcos[i]->get_local_x(), barcos[i]->get_local_y() + ii, 2, barcos[i]->get_orienta(), 1, 2);
                                        submarinos.push_back(submarino);
                                }
                                else if (barcos[i]->get_comprimento() == 4 && ii!=0)
                                {
                                        Porta_aviao *porta_aviao = new Porta_aviao(barcos[i]->get_local_x(), barcos[i]->get_local_y() + ii, 3, barcos[i]->get_orienta(), 50, 4);
                                        porta_avioes.push_back(porta_aviao);
                                }
                        }
                        break;
                default:
                        mapa_oculto[barcos[i]->get_local_y()][barcos[i]->get_local_x()] = barcos[i]->get_valor_mapa(); //caso seja uma canoa
                        break;
                }
        }
}

void Mapa::imprime_mapa_oculto()
{
        cout << "     ";
        for (conta1 = 0; conta1 < 13; conta1++)
        {
                printf("%.2d", conta1);
                cout << "   ";
        }

        cout << endl
             << endl;

        for (conta1 = 0, letra = 65; conta1 < 13; conta1++)
        {
                printf("%c", letra);
                letra++;
                cout << "    ";
                for (conta2 = 0; conta2 < 13; conta2++)
                {
                        cout << mapa_oculto[conta1][conta2] << "    ";
                }
                cout << endl
                     << endl;
        }
}

void Mapa::imprime_mapa_visivel()
{
        cout << "     ";
        for (conta1 = 0; conta1 < 13; conta1++)
        {
                printf("%.2d", conta1);
                cout << "   ";
        }

        cout << endl
             << endl;

        for (conta1 = 0, letra = 65; conta1 < 13; conta1++)
        {
                printf("%c", letra);
                letra++;
                cout << "    ";
                for (conta2 = 0; conta2 < 13; conta2++)
                {
                        cout << mapa_visivel[conta1][conta2] << "    ";
                }
                cout << endl
                     << endl;
        }
}

bool Mapa::colisao(int y, int x)
{
        unsigned int i;
        bool aleat;

        switch (mapa_oculto[y][x])
        {
        case -1:
                cout << "Ja foi acertado" << endl;
                return false;
                break;

        case 0:
                cout << "Acertou na agua" << endl;
                mapa_oculto[y][x] = -1;
                mapa_visivel[y][x] = 'O';
                return true;
                break;

        case 1:
                i = verifica_vector(y, x, 1);
                aleat = numero_aleatorio(canoas[i]->get_hate_mode());

                if (aleat)
                        cout << "A canoa foi detonada com dignidade" << endl;
                else
                        cout << "A canoa foi detonada miseravelmente" << endl;

                canoas[i]->set_valor_mapa(0);
                mapa_oculto[y][x] = -1;
                mapa_visivel[y][x] = 'X';
                return true;
                break;

        case 2:
                i = verifica_vector(y, x, 2);
                if (submarinos[i]->get_escudo() == 0)
                {
                        mapa_oculto[y][x] = -1;
                        mapa_visivel[y][x] = 'X';
                        submarinos[i]->set_valor_mapa(0);
                        cout << "O submarino foi obliterado desse universo" << endl;
                }
                else
                {
                        submarinos[i]->set_escudo(submarinos[i]->get_escudo() - 1);
                        cout << "O submarino conseguiu sobreviver ao seu ataque lixo" << endl;
                        mapa_visivel[y][x] = 'S';
                }
                return true;
                break;

        case 3:
                i = verifica_vector(y, x, 3);
                aleat = numero_aleatorio(porta_avioes[i]->get_hit_chance());

                if (aleat)
                {
                        cout << "O porta aviao foi destruido" << endl;
                        mapa_oculto[y][x] = -1;
                        mapa_visivel[y][x] = 'X';
                        porta_avioes[i]->set_valor_mapa(0);
                }
                else
                {
                        cout << "O porta aviao nao liga para seu missel" << endl;
                        mapa_visivel[y][x] = 'P';
                }

                return true;
                break;

        default:
                cout << "entrada invalida" << endl;
                exit(0);
                break;
        }
}

int Mapa::verifica_vector(int y, int x, int t)
{
        int aux1, aux2;
        unsigned int i, ii;
        switch (t)
        {
        case 1:
                for (ii = 0; ii < canoas.size(); ii++)
                {
                        aux1 = canoas[ii]->get_local_y();
                        aux2 = canoas[ii]->get_local_x();
                        if (aux1 == y && aux2 == x)
                                i = ii;
                }
                break;

        case 2:
                for (ii = 0; ii < submarinos.size(); ii++)
                {
                        aux1 = submarinos[ii]->get_local_y();
                        aux2 = submarinos[ii]->get_local_x();
                        if (aux1 == y && aux2 == x)
                                i = ii;
                }
                break;

        case 3:
                for (ii = 0; ii < porta_avioes.size(); ii++)
                {
                        aux1 = porta_avioes[ii]->get_local_y();
                        aux2 = porta_avioes[ii]->get_local_x();
                        if (aux1 == y && aux2 == x)
                                i = ii;
                }
                break;
        }
        return i;
}

bool Mapa::numero_aleatorio(int aux)
{
        int aleat;

        srand(time(NULL));
        aleat = (rand() % 100) + 1;
        if (aleat <= aux)
                return true;
        else
                return false;
}

bool Mapa::verifica_vitoria()
{
        unsigned int ii;
        for (ii = 0; ii < porta_avioes.size(); ii++)
        {
                if (porta_avioes[ii]->get_valor_mapa() != 0)
                {
                        //cout << "ainda tem pa" << endl;
                        return false;
                }
        }
        for (ii = 0; ii < submarinos.size(); ii++)
        {
                if (submarinos[ii]->get_valor_mapa() != 0)
                {
                        //cout << "ainda tem sub" << endl;
                        return false;
                }
        }
        for (ii = 0; ii < canoas.size(); ii++)
        {
                if (canoas[ii]->get_valor_mapa() != 0)
                {
                        //cout << "ainda tem canoa" << endl;
                        return false;
                }
        }
        return true;
}