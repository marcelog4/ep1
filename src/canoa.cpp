#include "canoa.hpp"
#include <iostream>

Canoa::Canoa()
{
        //cout << "Construtor da classe canoa" << endl;
}
Canoa::Canoa(int x, int y, int valor_mapa, char orienta, int hate_mode, int comprimento)
{
        set_local_x(x);
        set_local_y(y);
        set_valor_mapa(valor_mapa);
        set_orienta(orienta);
        set_comprimento(comprimento);
        set_hate_mode(hate_mode);
}
Canoa::~Canoa()
{
        //cout << "Destrutor da classe canoa" << endl;
}

int Canoa::get_hate_mode()
{
        return hate_mode;
}
void Canoa::set_hate_mode(int hate_mode)
{
        this->hate_mode = hate_mode;
}
