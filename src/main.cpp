#include <iostream>
#include <unistd.h>
#include "jogador.hpp"
#include "humano.hpp"
#include "computador.hpp"
#include "barco.hpp"
#include "submarino.hpp"
#include "porta_aviao.hpp"
#include "mapa.hpp"
#include "menu.hpp"

using namespace std;

int main()
{
        bool ganhou = false;
        int n = 0;
        Menu menu;
        Mapa mapa;

        menu.set_dificuldade(2);
        menu.escolha();

        Humano humano1;
        humano1.set_jogador_n(1);
        humano1.inicia_jogador();

        Computador computador1;
        computador1.set_jogador_n(1);
        computador1.inicia_jogador(menu.get_dificuldade());

        Humano humano2;
        humano2.set_jogador_n(2);
        humano2.inicia_jogador();

        Computador computador2;
        computador2.set_jogador_n(2);
        computador2.inicia_jogador(menu.get_dificuldade());

        system("tput reset");
        usleep(10000);

        while (!ganhou)
        {
                if (n % 2 == 0)
                {
                        if (menu.get_opcao() == 1 || menu.get_opcao() == 2)
                        {
                                ganhou = humano1.turno();
                        }
                        else if (menu.get_opcao() == 3)
                        {
                                ganhou = computador1.turno();
                        }
                }
                else
                {
                        if (menu.get_opcao() == 1)
                        {
                                ganhou = humano2.turno();
                        }
                        else if (menu.get_opcao() == 2 || menu.get_opcao() == 3)
                        {
                                ganhou = computador2.turno();
                        }
                }
                n++;
        }
        system("tput reset");
        return 0;
}