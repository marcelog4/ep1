#include "barco.hpp"
#include <iostream>

Barco::Barco(){
        comprimento = 0;
        valor_mapa = 0;
        local_x=0;
        local_y=0;
        orienta='\0';
        //cout << "Construtor da classe barco" << endl;
}

Barco::~Barco(){
        //cout << "Destrutor da classe barco" << endl;
}

int Barco::get_comprimento(){
        return comprimento;
}
void Barco::set_comprimento(int comprimento){
        this->comprimento = comprimento;
}

int Barco::get_valor_mapa(){
        return valor_mapa;
}
void Barco::set_valor_mapa(int valor_mapa){
        this->valor_mapa = valor_mapa;
}

int Barco::get_local_x(){
        return local_x;
}
void Barco::set_local_x(int local_x){
        this->local_x= local_x;
}

int Barco::get_local_y(){
        return local_y;
}
void Barco::set_local_y(int local_y){
        this->local_y = local_y;
}

char Barco::get_orienta(){
        return orienta;
}
void Barco::set_orienta(char orienta){
        this->orienta = orienta;
}

