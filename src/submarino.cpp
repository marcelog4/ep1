#include "submarino.hpp"
#include <iostream>

Submarino::Submarino(){
    escudo=1;
    //cout << "Construtor da classe submarino" << endl;
}

Submarino::Submarino(int x, int y, int valor_mapa, char orienta, int escudo , int comprimento){
    set_local_x(x);
    set_local_y(y);
    set_valor_mapa(valor_mapa);
    set_orienta(orienta);
    set_comprimento(comprimento);
    set_escudo(escudo);
}

Submarino::~Submarino(){
    //cout << "Destrutor da classe submarino" << endl;
}

int Submarino::get_escudo(){
    return escudo;
}
void Submarino::set_escudo(int escudo){
    this->escudo = escudo;
}