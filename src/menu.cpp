#include <iostream>
#include "menu.hpp"
#include <unistd.h>

using namespace std;

Menu::Menu(){
    //cout << "menu construido" << endl;
}

Menu::~Menu(){
    //cout << "menu destruido" << endl;
}

void Menu::imprime_menu(){
    cout << endl;
	printf("\t\t╔══╗ ╔═══╗╔════╗╔═══╗╔╗   ╔╗ ╔╗╔═══╗     ╔═╗ ╔╗╔═══╗╔╗  ╔╗╔═══╗╔╗   \n");
	printf("\t\t║╔╗║ ║╔═╗║║╔╗╔╗║║╔═╗║║║   ║║ ║║║╔═╗║     ║║╚╗║║║╔═╗║║╚╗╔╝║║╔═╗║║║   \n");
	printf("\t\t║╚╝╚╗║║ ║║╚╝║║╚╝║║ ║║║║   ║╚═╝║║║ ║║     ║╔╗╚╝║║║ ║║╚╗║║╔╝║║ ║║║║   \n");
	printf("\t\t║╔═╗║║╚═╝║  ║║  ║╚═╝║║║ ╔╗║╔═╗║║╚═╝║     ║║╚╗║║║╚═╝║ ║╚╝║ ║╚═╝║║║ ╔╗\n");
	printf("\t\t║╚═╝║║╔═╗║  ║║  ║╔═╗║║╚═╝║║║ ║║║╔═╗║     ║║ ║║║║╔═╗║ ╚╗╔╝ ║╔═╗║║╚═╝║\n");
	printf("\t\t╚═══╝╚╝ ╚╝  ╚╝  ╚╝ ╚╝╚═══╝╚╝ ╚╝╚╝ ╚╝     ╚╝ ╚═╝╚╝ ╚╝  ╚╝  ╚╝ ╚╝╚═══╝\n");
	cout << endl << endl << endl;
	cout << "\t\t1-Jogar" << endl;
	cout << "\t\t2-Dificuldade" << endl;
	cout << "\t\t3-Sair" << endl;
}

void Menu::escolhe_inimigo(){
	cout << endl;
	printf("\t\t╔══╗ ╔═══╗╔════╗╔═══╗╔╗   ╔╗ ╔╗╔═══╗     ╔═╗ ╔╗╔═══╗╔╗  ╔╗╔═══╗╔╗   \n");
	printf("\t\t║╔╗║ ║╔═╗║║╔╗╔╗║║╔═╗║║║   ║║ ║║║╔═╗║     ║║╚╗║║║╔═╗║║╚╗╔╝║║╔═╗║║║   \n");
	printf("\t\t║╚╝╚╗║║ ║║╚╝║║╚╝║║ ║║║║   ║╚═╝║║║ ║║     ║╔╗╚╝║║║ ║║╚╗║║╔╝║║ ║║║║   \n");
	printf("\t\t║╔═╗║║╚═╝║  ║║  ║╚═╝║║║ ╔╗║╔═╗║║╚═╝║     ║║╚╗║║║╚═╝║ ║╚╝║ ║╚═╝║║║ ╔╗\n");
	printf("\t\t║╚═╝║║╔═╗║  ║║  ║╔═╗║║╚═╝║║║ ║║║╔═╗║     ║║ ║║║║╔═╗║ ╚╗╔╝ ║╔═╗║║╚═╝║\n");
	printf("\t\t╚═══╝╚╝ ╚╝  ╚╝  ╚╝ ╚╝╚═══╝╚╝ ╚╝╚╝ ╚╝     ╚╝ ╚═╝╚╝ ╚╝  ╚╝  ╚╝ ╚╝╚═══╝\n");
	cout << endl << endl << endl;
	cout << "\t\t1-Humano VS Humano" << endl;
	cout << "\t\t2-Humano VS Maquina" << endl;
	cout << "\t\t3-Maquina VS Maquina" << endl;
}

void Menu::escolhe_dificuldade(){
	cout << endl;
	printf("\t\t╔══╗ ╔═══╗╔════╗╔═══╗╔╗   ╔╗ ╔╗╔═══╗     ╔═╗ ╔╗╔═══╗╔╗  ╔╗╔═══╗╔╗   \n");
	printf("\t\t║╔╗║ ║╔═╗║║╔╗╔╗║║╔═╗║║║   ║║ ║║║╔═╗║     ║║╚╗║║║╔═╗║║╚╗╔╝║║╔═╗║║║   \n");
	printf("\t\t║╚╝╚╗║║ ║║╚╝║║╚╝║║ ║║║║   ║╚═╝║║║ ║║     ║╔╗╚╝║║║ ║║╚╗║║╔╝║║ ║║║║   \n");
	printf("\t\t║╔═╗║║╚═╝║  ║║  ║╚═╝║║║ ╔╗║╔═╗║║╚═╝║     ║║╚╗║║║╚═╝║ ║╚╝║ ║╚═╝║║║ ╔╗\n");
	printf("\t\t║╚═╝║║╔═╗║  ║║  ║╔═╗║║╚═╝║║║ ║║║╔═╗║     ║║ ║║║║╔═╗║ ╚╗╔╝ ║╔═╗║║╚═╝║\n");
	printf("\t\t╚═══╝╚╝ ╚╝  ╚╝  ╚╝ ╚╝╚═══╝╚╝ ╚╝╚╝ ╚╝     ╚╝ ╚═╝╚╝ ╚╝  ╚╝  ╚╝ ╚╝╚═══╝\n");
	cout << endl << endl << endl;
	cout << "\t\t1-Facil" << endl;
	cout << "\t\t2-Medio" << endl;
	cout << "\t\t3-Dificil" << endl;
	cout << "\t\t4-Expert" << endl;
}

int Menu::get_opcao(){
    return opcao;
}
void Menu::set_opcao(int opcao){
    this->opcao = opcao;
}
int Menu::get_dificuldade(){
	return dificuldade;
}
void Menu::set_dificuldade(int dificuldade){
	this->dificuldade = dificuldade;
}

void Menu::opcao_usuario(){
	int aux;
	cin >> aux;
	set_opcao(aux);
	fflush(stdin);
}

void Menu::escolha(){
	system("tput reset");
	sleep(1);
	imprime_menu();
	opcao_usuario();
	switch (get_opcao())
	{
		case 1:
			system("tput reset");
			sleep(1);
			escolhe_inimigo();
			opcao_usuario();
		break;

		case 2:
			system("tput reset");
			sleep(1);
			escolhe_dificuldade();
			opcao_usuario();
			set_dificuldade(get_opcao());
			escolha();
		break;

		case 3:
			cout << "\033[2J";
			exit(0);
		break;

		default:
			cout << "\033[2J";
			cout << "faça uma escolha válida!" << endl;
			usleep(3000000);
			cout << "\033[2J";
			escolha();
		break;
	}
}

void Menu::imprime_dados(){
    cout << "A opcao foi: " << get_opcao() << endl;
}