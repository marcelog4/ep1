#include "porta_aviao.hpp"
#include <iostream>

Porta_aviao::Porta_aviao()
{
        hit_chance = 100;
        //cout << "Construtor da classe porta aviao" << endl;
}
Porta_aviao::Porta_aviao(int x, int y, int valor_mapa, char orienta, int hit_chance, int comprimento)
{
        set_local_x(x);
        set_local_y(y);
        set_valor_mapa(valor_mapa);
        set_orienta(orienta);
        set_comprimento(comprimento);
        set_hit_chance(hit_chance);
}

Porta_aviao::~Porta_aviao()
{
       // cout << "Destrutor da classe porta aviao" << endl;
}

int Porta_aviao::get_hit_chance()
{
        return hit_chance;
}
void Porta_aviao::set_hit_chance(int hit_chance)
{
        this->hit_chance = hit_chance;
}