#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include "jogador.hpp"
#include "mapa.hpp"

using namespace std;

Jogador::Jogador()
{
    //cout << "Construtor da classe jogador" << endl;
}
Jogador::~Jogador()
{
    //cout << "Destrutor da classe jogador" << endl;
}

int Jogador::get_jogador_n()
{
    return jogador_n;
}
void Jogador::set_jogador_n(int jogador_n)
{
    this->jogador_n = jogador_n;
}
int Jogador::get_tecla_x()
{
    return tecla_x;
}
void Jogador::set_tecla_x(int tecla_x)
{
    this->tecla_x = tecla_x;
}
int Jogador::get_tecla_y()
{
    return tecla_y;
}
void Jogador::set_tecla_y(int tecla_y)
{
    this->tecla_y = tecla_y;
}

void Jogador::inicia_jogador()
{
    //metodo virtual
}

int Jogador::configura_input(char aux)
{
    //metodo virtual
    return aux;
}

void Jogador::input_jogador()
{
    //metodo virtual
}

bool Jogador::turno()
{
    bool colidiu, venceu;
    //mapa.imprime_mapa_oculto();
    do{
    mapa.imprime_mapa_visivel();
    input_jogador();
    colidiu = mapa.colisao(get_tecla_y(), get_tecla_x());
    sleep(2);
    system("clear");
    }while(!colidiu);

    venceu = mapa.verifica_vitoria();
    if (venceu)
    {   
        usleep(30000);
        system("tput reset");
        usleep(30000);
        ganhou();
        usleep(3000000);
        return true;
    }

    sleep(1);
    return false;
}

void Jogador::ganhou(){
    if(get_jogador_n()==1){
        cout << "╔═══╗╔╗───╔═══╗╔╗──╔╗╔═══╗╔═══╗     ─╔╗─     ╔═══╗╔═══╗╔═╗─╔╗╔╗─╔╗╔═══╗╔╗─╔╗" << endl;
        cout << "║╔═╗║║║───║╔═╗║║╚╗╔╝║║╔══╝║╔═╗║     ╔╝║─     ║╔═╗║║╔═╗║║║╚╗║║║║─║║║╔═╗║║║─║║" << endl;
        cout << "║╚═╝║║║───║║─║║╚╗╚╝╔╝║╚══╗║╚═╝║     ╚╗║─     ║║─╚╝║║─║║║╔╗╚╝║║╚═╝║║║─║║║║─║║" << endl;
        cout << "║╔══╝║║─╔╗║╚═╝║─╚╗╔╝─║╔══╝║╔╗╔╝     ─║║─     ║║╔═╗║╚═╝║║║╚╗║║║╔═╗║║║─║║║║─║║" << endl;
        cout << "║║───║╚═╝║║╔═╗║──║║──║╚══╗║║║╚╗     ╔╝╚╗     ║╚╩═║║╔═╗║║║─║║║║║─║║║╚═╝║║╚═╝║" << endl;
        cout << "╚╝───╚═══╝╚╝─╚╝──╚╝──╚═══╝╚╝╚═╝     ╚══╝     ╚═══╝╚╝─╚╝╚╝─╚═╝╚╝─╚╝╚═══╝╚═══╝" << endl;
    }else
    if(get_jogador_n()==2){
        cout << "╔═══╗╔╗───╔═══╗╔╗──╔╗╔═══╗╔═══╗     ╔═══╗     ╔═══╗╔═══╗╔═╗─╔╗╔╗─╔╗╔═══╗╔╗─╔╗" << endl;
        cout << "║╔═╗║║║───║╔═╗║║╚╗╔╝║║╔══╝║╔═╗║     ║╔═╗║     ║╔═╗║║╔═╗║║║╚╗║║║║─║║║╔═╗║║║─║║" << endl;
        cout << "║╚═╝║║║───║║─║║╚╗╚╝╔╝║╚══╗║╚═╝║     ╚╝╔╝║     ║║─╚╝║║─║║║╔╗╚╝║║╚═╝║║║─║║║║─║║" << endl;
        cout << "║╔══╝║║─╔╗║╚═╝║─╚╗╔╝─║╔══╝║╔╗╔╝     ╔═╝╔╝     ║║╔═╗║╚═╝║║║╚╗║║║╔═╗║║║─║║║║─║║" << endl;
        cout << "║║───║╚═╝║║╔═╗║──║║──║╚══╗║║║╚╗     ║║╚═╗     ║╚╩═║║╔═╗║║║─║║║║║─║║║╚═╝║║╚═╝║" << endl;
        cout << "╚╝───╚═══╝╚╝─╚╝──╚╝──╚═══╝╚╝╚═╝     ╚═══╝     ╚═══╝╚╝─╚╝╚╝─╚═╝╚╝─╚╝╚═══╝╚═══╝" << endl;
    }
}