#include <iostream>
#include "computador.hpp"

Computador::Computador()
{
    // cout << "Construtor da classe computador" << endl;
}
Computador::~Computador()
{
    // cout << "Destrutor da classe computador" << endl;
}
int Computador::get_dificuldade()
{
    return dificuldade;
}
void Computador::set_dificuldade(int dificuldade)
{
    this->dificuldade = dificuldade;
}

void Computador::input_jogador()
{
    switch (get_dificuldade())
    {
    case 1:
        facil();
        break;

    case 2:
        medio();
        break;

    case 3:
        dificil();
        break;

    case 4:
        expert();
        break;

    default:
        cout << "deu ruim ne" << endl;
        exit(0);
        break;
    }
}

int Computador::facil()
{
    copia_matriz();
    srand(time(NULL));
    set_tecla_x(rand() % 13);
    set_tecla_y(rand() % 13);
    return 0;
}

int Computador::medio()
{
    copia_matriz();
    srand(time(NULL));

    for (i = 0; i < 13; i++)
    {
        for (ii = 0; ii < 13; ii++)
        {
            if (matriz_visivel_aux[i][ii] == 'S' || matriz_visivel_aux[i][ii] == 'P') //atira em submarino ou porta aviao ja encontrado
            {
                set_tecla_x(ii);
                set_tecla_y(i);
                return 0;
            }
        }
    }

    do
    {
        set_tecla_x(rand() % 13);
        set_tecla_y(rand() % 13);
    } while (matriz_visivel_aux[get_tecla_y()][get_tecla_x()] == 'X' || matriz_visivel_aux[get_tecla_y()][get_tecla_x()] == 'O'); //proibe o tiro em locais ja acertados
    return 0;
}

int Computador::dificil()
{
    copia_matriz();
    srand(time(NULL));

    for (i = 0; i < 13; i++)
    {
        for (ii = 0; ii < 13; ii++)
        {
            if (matriz_visivel_aux[i][ii] == 'S' && (matriz_visivel_aux[i + 1][ii] != 'S' || matriz_visivel_aux[i - 1][ii] != 'S' || matriz_visivel_aux[i][ii + 1] != 'S' || matriz_visivel_aux[i][ii - 1] != 'S'))
            {
                if (matriz_visivel_aux[i + 1][ii] == '~')
                {
                    set_tecla_x(ii);
                    set_tecla_y(i + 1);
                    return 0;
                }
                else if (matriz_visivel_aux[i - 1][ii] == '~')
                {
                    set_tecla_x(ii); //atira nas laterais do submarino
                    set_tecla_y(i - 1);
                    return 0;
                }
                else if (matriz_visivel_aux[i][ii + 1] == '~')
                {
                    set_tecla_x(ii + 1);
                    set_tecla_y(i);
                    return 0;
                }
                else if (matriz_visivel_aux[i][ii - 1] == '~')
                {
                    set_tecla_x(ii - 1);
                    set_tecla_y(i);
                    return 0;
                }
            }
        }
    }

    for (i = 0; i < 13; i++)
    {
        for (ii = 0; ii < 13; ii++)
        {
            if (matriz_visivel_aux[i][ii] == 'P' && (matriz_visivel_aux[i + 1][ii] != 'P' || matriz_visivel_aux[i - 1][ii] != 'P' || matriz_visivel_aux[i][ii + 1] != 'P' || matriz_visivel_aux[i][ii - 1] != 'P'))
            {
                if (matriz_visivel_aux[i + 1][ii] == '~')
                {
                    set_tecla_x(ii);
                    set_tecla_y(i + 1);
                    return 0;
                }
                else if (matriz_visivel_aux[i - 1][ii] == '~')
                {
                    set_tecla_x(ii); //atira nas laterais do porta aviao
                    set_tecla_y(i - 1);
                    return 0;
                }
                else if (matriz_visivel_aux[i][ii + 1] == '~')
                {
                    set_tecla_x(ii + 1);
                    set_tecla_y(i);
                    return 0;
                }
                else if (matriz_visivel_aux[i][ii - 1] == '~')
                {
                    set_tecla_x(ii - 1);
                    set_tecla_y(i);
                    return 0;
                }
            }
        }
    }

    for (i = 0; i < 13; i++)
    {
        for (ii = 0; ii < 13; ii++)
        {
            if (matriz_visivel_aux[i][ii] == 'P' && matriz_visivel_aux[i + 1][ii] == 'P' && matriz_visivel_aux[i + 2][ii] == '~')
            {
                set_tecla_x(ii);
                set_tecla_y(i + 2);
                return 0;
            }
            else if (matriz_visivel_aux[i][ii] == 'P' && matriz_visivel_aux[i - 1][ii] == 'P' && matriz_visivel_aux[i - 2][ii] == '~')
            {
                set_tecla_x(ii); //atira nas laterais do porta aviao
                set_tecla_y(i - 2);
                return 0;
            }
            else if (matriz_visivel_aux[i][ii] == 'P' && matriz_visivel_aux[i][ii + 1] == 'P' && matriz_visivel_aux[i][ii + 2] == '~')
            {
                set_tecla_x(ii + 2);
                set_tecla_y(i);
                return 0;
            }
            else if (matriz_visivel_aux[i][ii] == 'P' && matriz_visivel_aux[i][ii - 1] == 'P' && matriz_visivel_aux[i][ii - 2] == '~')
            {
                set_tecla_x(ii - 2);
                set_tecla_y(i);
                return 0;
            }
        }
    }

    for (i = 0; i < 13; i++)
    {
        for (ii = 0; ii < 13; ii++)
        {
            if (matriz_visivel_aux[i][ii] == 'S' || matriz_visivel_aux[i][ii] == 'P') //atira em submarino ou porta aviao ja encontrado
            {
                set_tecla_x(ii);
                set_tecla_y(i);
                return 0;
            }
        }
    }

    do
    {
        set_tecla_x(rand() % 13);
        set_tecla_y(rand() % 13);
    } while (matriz_visivel_aux[get_tecla_y()][get_tecla_x()] == 'X' || matriz_visivel_aux[get_tecla_y()][get_tecla_x()] == 'O'); //proibe o tiro em locais ja acertados
    return 0;
}

int Computador::expert()
{
    copia_matriz();
    for (i = 0; i < 13; i++)
    {
        for (ii = 0; ii < 13; ii++)
        {
            if (matriz_oculta_aux[i][ii] != 0 && matriz_oculta_aux[i][ii] != -1) //atira em submarino ou porta aviao ja encontrado
            {
                set_tecla_x(ii);
                set_tecla_y(i);
                return 0;
            }
        }
    }
    return 0;
}

void Computador::copia_matriz()
{
    for (i = 0; i < 13; i++)
        for (ii = 0; ii < 13; ii++)
            matriz_oculta_aux[i][ii] = mapa.mapa_oculto[i][ii];

    for (i = 0; i < 13; i++)
        for (ii = 0; ii < 13; ii++)
            matriz_visivel_aux[i][ii] = mapa.mapa_visivel[i][ii];
}

void Computador::inicia_jogador(int dificulade)
{
    mapa.ler_mapa(get_jogador_n());
    set_dificuldade(dificulade);
}