#include "humano.hpp"
#include <iostream>

Humano::Humano()
{
        //cout << "Construtor da classe humano" << endl;
}
Humano::~Humano()
{
        //cout << "Destrutor da classe canoa" << endl;
}

int Humano::configura_input(char aux)
{
    int y;
    y = (int)aux;
    if (y >= 97 && y <= 122)
        y -= 97;
    if (y >= 65 && y <= 90)
        y -= 65;

    return y;
}

void Humano::input_jogador()
{
    fflush(stdin);
    char aux;
    y = 0;
    x = 0;
    cout << "Digite a coordenada: " << endl;
    cin >> aux >> x;
    y = configura_input(aux);
    set_tecla_x(x);
    set_tecla_y(y);
    fflush(stdin);
}

void Humano::inicia_jogador()
{
    mapa.ler_mapa(get_jogador_n());
}