
## Descrição

Este projeto tem como objetivo executar um estilo de jogo semelhante a um batalha naval , porém com embarcações que apresentam skills, tornando
a jogabilidade um pouco diferente de um batalha naval comum.

## Instruções

Para execução é necessário que o player de a opção escolhida no menu , e seguinte a isso digite as coordenadas escolhidas na seguinte ordem
uma letra maiuscula ou minuscula e seguinte a ela , sucedida de espaço um inteiro.
O mapa tem dimensão 13x13 por padrão e 12 barcos para cada jogador.
O jogador pode disputar com outro player ou a maquina.


